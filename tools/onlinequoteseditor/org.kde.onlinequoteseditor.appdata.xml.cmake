<?xml version="1.0" encoding="utf-8"?>
<component type="desktop-application">
  <id>org.kde.onlinequoteseditor@TARGET_SUFFIX@</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <name>Online Quotes Editor</name>
  <name xml:lang="ca">Editor de cotitzacions en línia</name>
  <name xml:lang="ca-valencia">Editor de cotitzacions en línia</name>
  <name xml:lang="de">Editor für Online-Kursnotizen</name>
  <name xml:lang="en-GB">Online Quotes Editor</name>
  <name xml:lang="es">Editor de cotizaciones en línea</name>
  <name xml:lang="fr">Éditeur de cotations en ligne</name>
  <name xml:lang="ia">Editor de citationes in linea</name>
  <name xml:lang="it">Editor delle quotazioni in linea</name>
  <name xml:lang="ka">ონლაინ კოტების რედაქტორი</name>
  <name xml:lang="ko">온라인 시세 편집기</name>
  <name xml:lang="nl">Bewerker voor Online koersen</name>
  <name xml:lang="pt">Editor de Cotações 'Online'</name>
  <name xml:lang="pt-BR">Editor de cotações online</name>
  <name xml:lang="ru">Редактор котировок в интернете</name>
  <name xml:lang="sk">Editor online ponúk</name>
  <name xml:lang="sl">Urejevalnik spletnih kotacij</name>
  <name xml:lang="sv">Direktkurseditor</name>
  <name xml:lang="tr">Çevrimiçi Alıntı Düzenleyici</name>
  <name xml:lang="uk">Редактор інтернет-курсів</name>
  <name xml:lang="x-test">xxOnline Quotes Editorxx</name>
  <summary>Editor for online price quotes used by finance applications</summary>
  <summary xml:lang="ca">Editor per a la cotització en línia dels preus usat per les aplicacions financeres</summary>
  <summary xml:lang="ca-valencia">Editor per a la cotització en línia dels preus utilitzat per les aplicacions financeres</summary>
  <summary xml:lang="de">Editor für Online-Kursnotizen für Finanz-Anwendungen</summary>
  <summary xml:lang="en-GB">Editor for online price quotes used by finance applications</summary>
  <summary xml:lang="es">Editor de cotizaciones de precios en línea usado por aplicaciones de finanzas</summary>
  <summary xml:lang="fr">Éditeur pour les cotations en ligne utilisées par les applications financières</summary>
  <summary xml:lang="ia">Editor per citationes de prcio in linea usate per applicationes de financia</summary>
  <summary xml:lang="it">Editor delle quotazioni in linea usato delle applicazioni finanziarie</summary>
  <summary xml:lang="ka">ფინანსური აპლიკაციების მიერ გამოყენებული ონლაინ ფასების კოტების რედაქტორი</summary>
  <summary xml:lang="ko">재무 프로그램에서 사용하는 온라인 가격 시세 편집기</summary>
  <summary xml:lang="nl">Bewerker voor online koersen gebruikt door financiële toepassingen</summary>
  <summary xml:lang="pt">Editor de cotações de preços 'online' usados pelas aplicações financeiras</summary>
  <summary xml:lang="pt-BR">Editor de cotações de preços online usado por aplicativos de finanças</summary>
  <summary xml:lang="sk">Editor pre online cenové ponuky používané finančnými aplikáciami</summary>
  <summary xml:lang="sl">Urednik spletnih kotacij, ki jih uporabljajo finančne aplikacije</summary>
  <summary xml:lang="sv">Editor för direktkurser använd av ekonomiprogram</summary>
  <summary xml:lang="tr">Finans uygulamaları tarafından kullanılan çevrimiçi fiyat alıntıları düzenleyici</summary>
  <summary xml:lang="uk">Редактор для джерел курсів цін у інтернеті, який використовується фінансовими програмами</summary>
  <summary xml:lang="x-test">xxEditor for online price quotes used by finance applicationsxx</summary>
  <description>
    <p>Online Quotes Editor is a graphical user interface application for editing and checking online quotes. Available online quote sources can be added from the KDE store, created locally, edited and checked for correct execution.</p>
    <p xml:lang="ca">L'editor de cotitzacions en línia és una aplicació d'interfície d'usuari gràfica per editar i comprovar les cotitzacions en línia. Les fonts de cotització en línia disponibles es poden afegir des de la botiga KDE, creades localment, editades i verificades per a una execució correcta.</p>
    <p xml:lang="ca-valencia">L'editor de cotitzacions en línia és una aplicació d'interfície d'usuari gràfica per a editar i comprovar les cotitzacions en línia. Les fonts de cotització en línia disponibles es poden afegir des de la botiga KDE, creades localment, editades i verificades per a una execució correcta.</p>
    <p xml:lang="en-GB">Online Quotes Editor is a graphical user interface application for editing and checking online quotes. Available online quote sources can be added from the KDE store, created locally, edited and checked for correct execution.</p>
    <p xml:lang="es">El editor de cotizaciones en línea es una aplicación con interfaz gráfica para editar y comprobar cotizaciones en línea. Puede añadir las fuentes disponibles de cotizaciones en línea desde la tienda de KDE, creadas localmente, modificadas y comprobadas para que se ejecuten correctamente.</p>
    <p xml:lang="fr">L'éditeur de cotations en ligne est une application avec une interface utilisateur graphique permettant d'éditer et de vérifier des cotations en ligne. Les sources de cotations disponibles en ligne peuvent être ajoutées à partir de la boutique de KDE, créées localement, modifiées et vérifiées pour une exécution correcte.</p>
    <p xml:lang="ia">Editor de Citationes in Linea es un application de interfacie de usator pro modificar e verificar citationes in linea. Fontes de citation disponibile in linea pote esser addite ab le magazin de KDE, create  localmente, modificate e verificate per un correcte execution.</p>
    <p xml:lang="it">Editor delle quotazioni in linea è un'interfaccia utente grafica per la modifica e il controllo delle quotazioni in linea. È possibile aggiungere le fonti di quotazioni disponibili in rete dal negozio di KDE, crearle localmente, modificarle e verificarne la corretta esecuzione.</p>
    <p xml:lang="ko">온라인 시세 편집기는 온라인 시세를 편집하고 확인하는 그래픽 사용자 인터페이스 프로그램입니다. 사용 가능한 온라인 시세 정보는 KDE 스토어에서 추가, 로컬에서 생성, 정확한 실행을 위해서 편집 및 확인할 수 있습니다.</p>
    <p xml:lang="nl">Bewerker voor Online koersen is een grafische gebruikersinterfacetoepassing voor bewerken en controleren van online koersen. Beschikbare online koersbronnen kunnen toegevoegd worden uit de KDE-store, lokaal aangemaakt, bewerkt en gecontroleerd voor het juiste gebruik.</p>
    <p xml:lang="pt">O Editor de Cotações Online é uma aplicação gráfica para editar e consultar cotações 'online'. Poderá adicionar fontes adicionais a partir da loja do KDE, criadas a nível local, assim como editar ou consultar outra para uma execução correcta.</p>
    <p xml:lang="sl">Urejevalnik spletnih kotacij je program z grafičnim uporabniškim vmesnikom za urejanje in preverjanje spletnih kotacij. Razpoložljive vire spletnih kotacij lahko dodate iz trgovine KDE, jih ustvarite krajevno ter urejate in preverjate pravilno izvajanje.</p>
    <p xml:lang="sv">Direktkurseditor är ett grafiskt användargränssnitt för att redigera och kontrollera kurser på nätet. Tillgängliga källor för kurser kan läggas till från KDE:s butik, skapas lokalt, redigeras och kontrolleras så att de kör korrekt.</p>
    <p xml:lang="tr">Çevrimiçi Alıntı Düzenleyici, çevrimiçi alıntıları düzenlemek ve denetlemek için kullanılan bir uygulamadır. Çevrimiçi alıntı kaynakları KDE mağazasından indirilebilir, yerel olarak oluşturulabilir ve doğru çalışmaları için denetlenebilir.</p>
    <p xml:lang="uk">«Редактор інтернет-курсів» — програма із графічним інтерфейсом для редагування та отримання даних інтернет-курсів. Доступні джерела інтернет-курсів можна додавати з крамниці KDE, створювати локально, редагувати та перевіряти на правильність функціонування.</p>
    <p xml:lang="x-test">xxOnline Quotes Editor is a graphical user interface application for editing and checking online quotes. Available online quote sources can be added from the KDE store, created locally, edited and checked for correct execution.xx</p>
    <p>The interface is divided into customizable sub-windows for profiles, profile details, assigned online quotes, quote attributes, debug, and browser, allowing for flexible interface setup.</p>
    <p xml:lang="ca">La interfície es divideix en subfinestres personalitzables per a perfils, detalls de perfil, cotitzacions assignades en línia, atributs de les cotitzacions, depuració i navegador, permetent la configuració flexible de la interfície.</p>
    <p xml:lang="ca-valencia">La interfície es dividix en subfinestres personalitzables per a perfils, detalls de perfil, cotitzacions assignades en línia, atributs de les cotitzacions, depuració i navegador, permetent la configuració flexible de la interfície.</p>
    <p xml:lang="en-GB">The interface is divided into customisable sub-windows for profiles, profile details, assigned online quotes, quote attributes, debug, and browser, allowing for flexible interface setup.</p>
    <p xml:lang="es">La interfaz está dividida en subventanas personalizables para perfiles, detalles de los perfiles, cotizaciones en línea asignadas, atributos de las cotizaciones, depuración de datos y navegación, que permiten una configuración flexible de la interfaz.</p>
    <p xml:lang="fr">L'interface est divisée en sous-fenêtres personnalisables pour les profils, les détails du profil, les cotations attribuées en ligne, les attributs de cotations, le débogage et le navigateur, permettant une configuration flexible de l'interface.</p>
    <p xml:lang="ia">Le interfacie es dividite in sub-fenestras personalisabile per profilos, detalios de profilo, citationes assignate in linea, attributos de citationes, cribrar(debug), e navigar ,permittente un configuration de interfacie flexibile.  </p>
    <p xml:lang="it">L'interfaccia è suddivisa in finestre secondarie personalizzabili per profili, dettagli del profilo, quotazioni in linea assegnate, attributi di quotazione, debug e browser, consentendo una configurazione flessibile dell'interfaccia.</p>
    <p xml:lang="ko">인터페이스는 프로필, 프로필 정보, 할당된 온라인 시세, 종목 속성, 디버그, 브라우저 등 사용자 정의 가능한 하위 창으로 나뉘어 있으며 인터페이스를 유연하게 설정할 수 있습니다.</p>
    <p xml:lang="nl">Het interface is opgedeeld in aanpasbare subvensters voor profielen, profieldetails, toegekende online koersen, attributen van koersen, debug en browser, waarmee een flexibele interfaceopzet wordt geboden.</p>
    <p xml:lang="pt">A interface está dividido em sub-janelas personalizadas para os perfis, detalhes dos perfis, cotações 'online' atribuídas, atributos das cotações, depuração e navegador, permitindo uma configuração flexível da interface.</p>
    <p xml:lang="sl">Vmesnik je razdeljen na prilagodljiva podokna za profile, podrobnosti profila, dodeljene spletne kotacije, atribute kotacij, odpravljanje napak in brskalnik, kar omogoča prilagodljivo nastavitev vmesnika.</p>
    <p xml:lang="sv">Gränssnittet är uppdelat i anpassningsbara delfönster för profiler, profilinformation, tilldelade direktkurser, kursegenskaper, felsökning, och webbläsare, vilket tillåter flexibel gränssnittsinställning.</p>
    <p xml:lang="tr">Arabirim; esnek bir arabirim kurulumu için profiller, profil ayrıntıları, atanmış çevrimiçi alıntılar, alıntı öznitelikleri, hata ayıklama ve tarayıcı için özelleştirilebilir alt pencerelere ayrılmıştır.</p>
    <p xml:lang="uk">Інтерфейс програми поділено на придатні до налаштовування підвікна для профілів, параметрів профілів, призначених інтернет-курсів, атрибутів курсів, діагностики та панель навігації. Усе це уможливлює гнучке налаштовування інтерфейсу.</p>
    <p xml:lang="x-test">xxThe interface is divided into customizable sub-windows for profiles, profile details, assigned online quotes, quote attributes, debug, and browser, allowing for flexible interface setup.xx</p>
    <p>Multiple applications can be supported via profiles.</p>
    <p xml:lang="ca">Poden funcionar aplicacions múltiples a través dels perfils.</p>
    <p xml:lang="ca-valencia">A través dels perfils s'admeten múltiples aplicacions.</p>
    <p xml:lang="en-GB">Multiple applications can be supported via profiles.</p>
    <p xml:lang="es">Se pueden usar múltiples aplicaciones mediante perfiles.</p>
    <p xml:lang="fr">Plusieurs applications peuvent être prises en charge grâce des profils.</p>
    <p xml:lang="ia">Applicationes multiple pote esser supportate via profilos. </p>
    <p xml:lang="it">È possibile supportare più applicazioni tramite profili.</p>
    <p xml:lang="ka">მხარდაჭერილი აპლიკაციების დამატება პროფილებით შეგიძლიათ.</p>
    <p xml:lang="ko">프로필을 사용하여 여러 프로그램을 지원합니다.</p>
    <p xml:lang="nl">Meerdere toepassingen kunnen ondersteund worden via profielen.</p>
    <p xml:lang="pt">Poderá ter suporte para várias aplicações através dos perfis.</p>
    <p xml:lang="pt-BR">Múltiplos aplicativos podem ser suportados através de perfis.</p>
    <p xml:lang="sl">Več aplikacij je mogoče podpreti prek profilov.</p>
    <p xml:lang="sv">Flera program kan stödjas via profiler.</p>
    <p xml:lang="tr">Profiller yardımı ile birden çok uygulama desteklenebilir.</p>
    <p xml:lang="uk">Передбачено можливість підтримки декількох програм із різними профілями.</p>
    <p xml:lang="x-test">xxMultiple applications can be supported via profiles.xx</p>
    <p>Financial applications that do not contain their own user interface for editing online quote sources can call the online quote editor for this purpose.</p>
    <p xml:lang="ca">Les aplicacions financeres que no tenen la seva pròpia interfície d'usuari per editar fonts de cotitzacions en línia poden invocar l'editor de cotitzacions en línia per a aquest propòsit.</p>
    <p xml:lang="ca-valencia">Les aplicacions financeres que no tenen la seua pròpia interfície d'usuari per a editar fonts de cotitzacions en línia poden invocar l'editor de cotitzacions en línia per a este propòsit.</p>
    <p xml:lang="en-GB">Financial applications that do not contain their own user interface for editing online quote sources can call the online quote editor for this purpose.</p>
    <p xml:lang="es">Las aplicaciones de finanzas que no contienen su propia interfaz para editar fuentes de cotizaciones en línea pueden llamar al editor de cotizaciones en línea para este propósito.</p>
    <p xml:lang="fr">Les applications financières ne possédant pas leur propre interface utilisateur pour modifier les sources de cotations en ligne peuvent appeler l'éditeur de cotation en ligne à cette fin.</p>
    <p xml:lang="ia">Applicationes finantiari que non contine lor proprie interfacie de usator per modificar fontes de citation in linea pote appellar le editor de citation in linea pro iste proposito.</p>
    <p xml:lang="it">Le applicazioni finanziarie che non contengono la propria interfaccia utente per la modifica delle fonti di quotazione in linea possono chiamare a questo scopo l'editor delle quotazioni in linea.</p>
    <p xml:lang="ko">자체 온라인 종목 원본 편집 인터페이스가 없는 재무 프로그램에서 온라인 시세 편집기를 이 목적으로 호출할 수 있습니다.</p>
    <p xml:lang="nl">Financiële toepassingen die geen eigen gebruikersinterface voor bewerken van online koersbronnen kunnen de online koersenbewerker voor dit doel aanroepen.</p>
    <p xml:lang="pt">As aplicações financeiras que não possuam a sua própria interface de utilizador para editar as fontes de cotações 'online' poderão invocar o editor de cotações 'online' para esse fim.</p>
    <p xml:lang="sl">Finančne aplikacije, ki ne vsebujejo lastnega uporabniškega vmesnika za urejanje virov spletnih kotacij, lahko v ta namen pokličejo urejevalnik spletnih kotacij.</p>
    <p xml:lang="sv">Ekonomiprogram som inte har sitt egna användargränssnitt för att redigera kurskällor på nätet kan anropa direktkurseditorn i detta syfte.</p>
    <p xml:lang="tr">Çevrimiçi fiyat alıntı kaynaklarını düzenlemek için bir arabirim sunmayan finansal uygulamalar bunun için bu uygulamayı kullanabilirler.</p>
    <p xml:lang="uk">Фінансові програми, у яких немає власного інтерфейсу для редагування джерел інтернет-курсів, можуть викликати з цією метою «Редактор інтернет-курсів».</p>
    <p xml:lang="x-test">xxFinancial applications that do not contain their own user interface for editing online quote sources can call the online quote editor for this purpose.xx</p>
  </description>
  <url type="homepage">https://community.kde.org/Alkimia</url>
  <url type="bugtracker">https://bugs.kde.org/enter_bug.cgi?format=guided%26product=libalkimia%26component=Online+Quotes+Editor</url>
  <url type="donation">https://www.kde.org/community/donations/?app=onlinequoteseditor</url>
  <launchable type="desktop-id">org.kde.onlinequoteseditor.desktop</launchable>
  <screenshots>
    <screenshot type="default">
      <image>https://cdn.kde.org/screenshots/onlinequoteseditor/onlinequoteseditor.png</image>
    </screenshot>
  </screenshots>
  <provides>
    <binary>onlinequoteseditor@TARGET_SUFFIX@</binary>
  </provides>
  <project_group>KDE</project_group>
  <releases>
    <release version="8.1.1" date="2022-05-03"/>
  </releases>
</component>
